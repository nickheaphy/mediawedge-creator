# Tool to Generate Media Wedges for i1Pro

Put a tab delimited `Color_Values.txt` file into the inc folder then run `generate_tif.py`

Generates a TIF file and .it8 file suitable for reading using the i1Pro2 spectrophotometer.

![Media Wedge](/inc/wedge_image.png)

# Configuration

Scripts are written in Python3 so you need this installed first.

Need the `PSOcoated_v3.icc` profile from http://www.eci.org/en/downloads copied to `inc` folder for the conversion.

Need [LittleCMS](http://www.littlecms.com/) installed for the ImageCMS functions in Pillow to work

```
brew install littlecms
```

Need Pillow installed within Python

```
pip install Pillow
```

There are some configuration options in the `generate_tif.py` to control the size and resolution of the resulting TIF file. There are some options for the generation of the seperator, but I have had some problems getting the i1Pro2 to read patches based on a dE<20. Your results may vary.