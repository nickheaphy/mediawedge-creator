import math
import PIL
from PIL import ImageCms
from PIL import Image
from PIL import ImageChops
from PIL import ImageDraw
import math
import datetime
from string import ascii_uppercase

# Configuration
# Size and space are in mm, resolution in DPI
size = 7
space = 1
difference = 20
length = 24
resolution = 300
min_dE = 1000 # Had some problems with detection even tweaking the dE and contrast values, this will force all to have seperators 
required_contrast = 50

cmyk_profile_file = "inc/PSOcoated_v3.icc"
colorvalues = "inc/Color_Values.txt"

lab_profile = ImageCms.createProfile("LAB")
cmyk_profile = ImageCms.getOpenProfile(cmyk_profile_file)
transform = ImageCms.buildTransformFromOpenProfiles(cmyk_profile,lab_profile,"CMYK","LAB")

# ---------------------------------------
def deltaE76(col1, col2):
    '''Returns the dE between two lab values'''
    return math.sqrt((col2[0]-col1[0])**2 + (col2[1]-col1[1])**2 + (col2[2]-col1[2])**2)

# Need to convert RGB to Lab?
# https://stackoverflow.com/questions/13405956/convert-an-image-rgb-lab-with-python

# ---------------------------------------
def deltaEbetweenCMYK(col1,col2):
    '''Expects two col1 (scaled values to 0-255) and will calculate the dE'''
    img1 = Image.new("CMYK", (1,1), col1)
    img2 = Image.new("CMYK", (1,1), col2)

    #convert it to LAB
    img1_lab = ImageCms.applyTransform(img1,transform)
    img2_lab = ImageCms.applyTransform(img2,transform)

    #get the new colour
    lab_col1 = img1_lab.getpixel((0,0))
    lab_col2 = img2_lab.getpixel((0,0))

    print("CMYK: {}".format(scale_cmykfrom255(col1)))
    print("LAB: {}".format(scale_255tolab(lab_col1)))
    print()

    #return the dE and the seperator
    return deltaE76(lab_col1,lab_col2)

# ---------------------------------------
def contrastbetweenCMYK(col1,col2):
    '''Figure out the contrast between colours'''
    #https://stackoverflow.com/questions/9733288/how-to-programmatically-calculate-the-contrast-ratio-between-two-colors
    col1_rgb = cmyk_to_rgb(col1,scaled=True)
    col2_rgb = cmyk_to_rgb(col2,scaled=True)
    brightness1 = (299*col1_rgb[0] + 587*col1_rgb[1] + 114*col1_rgb[2]) / 1000
    brightness2 = (299*col2_rgb[0] + 587*col2_rgb[1] + 114*col2_rgb[2]) / 1000
    return abs(brightness1-brightness2)

# ---------------------------------------
def cmyk_to_rgb(col,scaled=False):
    '''Really crude conversion from CMYK to RGB'''
    #https://stackoverflow.com/questions/14088375/how-can-i-convert-rgb-to-cmyk-and-vice-versa-in-python
    if scaled:
        rgb_scale = 255
        cmyk_scale = 100
    else:
        rgb_scale = 255
        cmyk_scale = 255
    
    r = rgb_scale*(1.0-(col[0]+col[3])/float(cmyk_scale))
    g = rgb_scale*(1.0-(col[1]+col[3])/float(cmyk_scale))
    b = rgb_scale*(1.0-(col[2]+col[3])/float(cmyk_scale))
    return (r,g,b)

# ---------------------------------------
def scale_cmykto255(color):
    '''expects a list of four numbers between 0-100, outputs between 0-255'''
    c_mod = int(color[0]/100*255)
    m_mod = int(color[1]/100*255)
    y_mod = int(color[2]/100*255)
    k_mod = int(color[3]/100*255)
    return (c_mod,m_mod,y_mod,k_mod)

# ---------------------------------------
def scale_cmykfrom255(color):
    '''expects a list of four numbers between 0-255, outputs between 0-100'''
    c_mod = int(color[0]/255*100)
    m_mod = int(color[1]/255*100)
    y_mod = int(color[2]/255*100)
    k_mod = int(color[3]/255*100)
    return (c_mod,m_mod,y_mod,k_mod)

# ---------------------------------------
def scale_255tolab(color):
    '''expects three numbers between 0 and 255 and scales to CIELAB
        L in [0, 100]
        A in [-86.185, 98.254]
        B in [-107.863, 94.482] (assume CIELAB)'''
        # (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
    #l = (((color[0] - 0.0) * (100.0 - 0.0)) / (255.0 - 0.0)) + 0.0
    l = (((color[0] - 0.0) * (100.0 - 0.0)) / (255.0 - 0.0)) + 0.0
    a = (((color[1] - 0.0) * (98.254 - -86.185)) / (255.0 - 0.0)) + -86.185
    #a = (color[1] * 184.439) / 168.815
    b = (((color[2] - 0.0) * (94.482 - -107.863)) / (255.0 - 0.0)) + -107.863
    # b = (color[2] * 202.345) / 147.137
    return (l,a,b)

# ---------------------------------------
def imgenlarge(img, extra, centre=False):
    '''add the extra space to the CMYK image and return'''
    # https://stackoverflow.com/questions/9631735/python-adding-extra-area-to-image?rq=1
    size = (img.size[0]+extra[0],img.size[1]+extra[1])
    layer = Image.new('CMYK', size, (0,0,0,0))
    if centre:
        layer.paste(img, tuple(map(lambda x:(x[0]-x[1])/2, zip(size, img.size))))
    else:
        layer.paste(img, 0,0)
    return layer

# ---------------------------------------
def imgtrim(im):
    '''trim off the border space (get border colour from bottom right pixel)'''
    #https://stackoverflow.com/questions/10615901/trim-whitespace-using-pil
    width, height = im.size
    bg = Image.new(im.mode, im.size, im.getpixel((width-1,height-1)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return im.crop(bbox)

# ---------------------------------------
def numlines(file):
    # for i, l in enumerate(file):
    #     pass
    # return i
    i = sum(1 for line in file)
    #reset the file pointer (otherwise the file will close)
    file.seek(0)
    return i

# ---------------------------------------
def main():
    # Open the Colour_Values file
    colvals = open(colorvalues,"r")

    # Create a oversize image to hold the chart (will trim down later)
    numberoflines = numlines(colvals)
    width = math.ceil((length * size + length * space) )
    height = math.ceil((numberoflines/length) * size)

    wedge = Image.new("CMYK",(width,height),(0,0,0,0))
    draw = ImageDraw.Draw(wedge)

    # loop through the file and get all the colours. Store in an array
    # need to 
    colourlist_scaled = []
    colourlist_unscaled = []
    for line in colvals:
        colour = line.strip().split("\t")
        scaled_col = scale_cmykto255((int(colour[0]),int(colour[1]),int(colour[2]),int(colour[3])))
        colourlist_scaled.append(scaled_col)
        colourlist_unscaled.append((int(colour[0]),int(colour[1]),int(colour[2]),int(colour[3])))
    
    # decide where we need to draw seperators
    sep_required = [0] * length
    sep_col = [0] * len(colourlist_scaled)
    for i, col in enumerate(colourlist_scaled):
        # skip the begining and end
        if i % length != 0 and i % length != length:
            dE = deltaEbetweenCMYK(col,colourlist_scaled[i-1])
            contrast = contrastbetweenCMYK(col,colourlist_scaled[i-1])
            print("Contrast: {}".format(contrast))
            if dE < min_dE:
                sep_required[i % length] = 1
            if contrast < required_contrast:
                sep_required[i % length] = 1
            # check if contrast better using black or white
            if contrastbetweenCMYK(col,(0,0,0,0)) < contrastbetweenCMYK(col,(0,0,0,255)):
                sep_col[i] = 1
            else:
                sep_col[i] = 2
            # check we don't put a black line next to a dark box
            if col == (0,0,0,0):
                sep_col[i] = 2

    #print("Seperators Required: {}".format(sep_required))

    # Now draw the wedge
    for y_offset in range(0,math.ceil((len(colourlist_scaled)/length))):

        yp = y_offset * size
        xp = 0

        for x_offset in range(0,length):

            # do we need a seperator 
            if sep_required[x_offset] > 0:
                # Need to draw a seperator
                if sep_col[x_offset+(y_offset*length)] == 2:
                    #white patch
                    draw.rectangle([(xp,yp),(xp+space,yp+size)],(0,0,0,0))
                else:
                    #black patch
                    draw.rectangle([(xp,yp),(xp+space,yp+size)],(0,0,0,255))
                # offset the x position
                xp += space
            
            # need to draw the box
            draw.rectangle([(xp,yp),(xp+size,yp+size)],colourlist_scaled[x_offset+(y_offset*length)])
            # offset the x position
            xp += size

    # clean up the extra white space
    trimmed = imgtrim(wedge)
    #scale up to required resolution
    new_width = int(trimmed.size[0]*resolution/25.4)
    new_height = int(trimmed.size[1]*resolution/25.4)
    big = trimmed.resize((new_width,new_height),Image.NEAREST)
    big.save("wedge_image.tif", dpi=(resolution, resolution))

    # now write the descriptor
    f = open("wedge_values.it8","w+")
    f.write('ORIGINATOR "https://bitbucket.org/nickheaphy/mediawedge-creator"\n')
    f.write('DESCRIPTOR "MediaWedge"\n')
    f.write('CREATED "{}"\n'.format(datetime.datetime.now()))
    f.write('NUMBER_OF_FIELDS 5\n')
    f.write('BEGIN_DATA_FORMAT\n')
    f.write('SAMPLE_ID CMYK_C CMYK_M CMYK_Y CMYK_K\n')
    f.write('END_DATA_FORMAT\n')
    f.write('DEVICE 22\n')
    f.write('NUMBER_OF_STRIPS {}\n'.format(math.ceil((len(colourlist_scaled)/length))))
    f.write('NUMBER_OF_COLS {}\n'.format(length))
    f.write('NUMBER_OF_SETS {}\n'.format(len(colourlist_scaled)))
    f.write('BEGIN_DATA\n')
    # for i, col in enumerate(colourlist_unscaled):
    #     f.write("{}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\n".format(i+1,col[0],col[1],col[2],col[3]))
    
    for y_offset in range(0,math.ceil((len(colourlist_scaled)/length))):
        for x_offset in range(0,length):
            rowletter = ascii_uppercase[y_offset]
            rownumber = x_offset+1
            col = colourlist_unscaled[x_offset+(y_offset*length)]
            f.write("{}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\n".format(rowletter+str(rownumber),col[0],col[1],col[2],col[3]))
    f.write('END_DATA')
    f.close()



if __name__ == "__main__":
    main()