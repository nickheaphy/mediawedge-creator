## i1Pro2 Chart Rules

From the [documentation](http://www.colorwave.it/_USRS/docs/ChartDesignRules.pdf)

* All patches in one row have the same width
* Before and after a row, the chart has to be white with a minimal distance of 12 mm
* A Chart row consists of 6 or more patches in scanning direction
* Separation bar width: 0.5mm - 1.0mm

### Charts measured with i1Pro2-Ruler (NO zebra)

* Minimal patch width: 10 mm
* Minimal patch height: 8 mm
* Patch arrangement:
   * DeltaE between all adjacent patches in scanning direction > 20 deltaE
   * **or** patches separated with separation bar white or black (whichever leads to higher contrast)

### Charts measured with i1Pro2-Ruler2 (WITH zebra)

* Minimal patch width: 7 mm
* Minimal patch height: 8 mm
* Patch arrangement:
   * More than 50% of adjacent patches in scanning direction need > 20 deltaE between each other
   * **or** patches separated with separation bar white or black (whichever leads to higher contrast)